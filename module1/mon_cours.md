LANGUAGE: fr
CSS: http://culturenumerique.univ-lille3.fr/css/base.css" />
TITLE: Coucou
MENUTITLE: Salut
AUTHOR: Célestine SAUVAGE

# Plop

##Mon premier cours
1. Je fais une liste
2. Avec des valeurs

Je mets du **gras** de *l'italique* et *d'autres _trucs_*

## Mettons des liens et des photos
[Lien de la mort qui tue](http://www.theuselessweb.com/)
![Chaton](media/chaton.jpg)

## Je rédige un quizz
```activité
::Quizz de l'extrême::
[markdown]
**Le matin :**
{ 
 ~%50% Je me lève
 ~%-50% Je me couche
 ~%50% Je mange & me lave les dents
 ~%-50% Je ne vais pas au travail
 #### Je me **lève**, je **mange** et me **lave les dents** évidemment !
 }
 

```
